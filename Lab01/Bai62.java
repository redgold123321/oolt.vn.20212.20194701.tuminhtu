import javax.swing.JOptionPane;
public class Bai62 {
  public static void main(String[] args){
    String a11,b11,c11,a21,b21,c21;

    String strNotification = "first-degree equations (linear system) with two variables";
    a11 = JOptionPane.showInputDialog(null,
                "Input a11",
                JOptionPane. INFORMATION_MESSAGE);
    double a1 = Double.parseDouble(a11);
    b11 = JOptionPane.showInputDialog(null,
                "Input b11",
                JOptionPane. INFORMATION_MESSAGE);  
                double b1 = Double.parseDouble(b11);
    c11 = JOptionPane.showInputDialog(null,
                "Input c11",
                JOptionPane. INFORMATION_MESSAGE);    
                double c1 = Double.parseDouble(c11);          
    a21 = JOptionPane.showInputDialog(null,
                "Input a21",
                JOptionPane. INFORMATION_MESSAGE);
                double a2 = Double.parseDouble(a21);
    b21 = JOptionPane.showInputDialog(null,
                "Input b21",
                JOptionPane. INFORMATION_MESSAGE);  
                double b2 = Double.parseDouble(b21);
    c21 = JOptionPane.showInputDialog(null,
                "Input c21",
                JOptionPane. INFORMATION_MESSAGE); 
                double c2 = Double.parseDouble(c21);                                      
    double bx=b1*a2;        
    double cx=c1*a2;       
    double by=b2*a1;        
    double cy=c2*a1;
        
    double y = (cy-cx)/(bx-by);
    System.out.println("y = "+y);
    double x = (-cx-bx*y)/(a1*a2);
    System.out.println("x = "+x);
    strNotification += "x =" + x +" and y="+ y;
    JOptionPane.showMessageDialog(null, strNotification,
                 "   ", JOptionPane. INFORMATION_MESSAGE);
    System.exit(0);
  }
}