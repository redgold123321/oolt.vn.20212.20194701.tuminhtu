/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author DELL
 */
import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    CreateMyDate myDate = new CreateMyDate();
    System.out.println("Today : ");
    myDate.print();

    Scanner Input = new Scanner(System.in);
    System.out.println("Enter a date : ");
    String inputDate = Input.nextLine();
    myDate = new CreateMyDate(inputDate);
    myDate.print();
    Input.close();
  }
}
