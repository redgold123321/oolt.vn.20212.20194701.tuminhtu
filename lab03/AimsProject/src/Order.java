/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author DELL
 */
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private int qtyOrdered = 0;
	private DigitalVideoDisc itemOrderd[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];

	public void addDigitalVideoDisc(DigitalVideoDisc disc) {

		if (qtyOrdered + 1 <= MAX_NUMBERS_ORDERED) {
			itemOrderd[qtyOrdered] = disc;
			this.qtyOrdered++;
			System.out.println("The disc has been added");
		} else {
			System.out.println("The order is almost full");
		}
	}

	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		for (int i = 0; i < qtyOrdered; i++) {
			if (itemOrderd[i].equals(disc)) {
				for (int j = i; j < qtyOrdered - 1; j++) {
					itemOrderd[j] = itemOrderd[j + 1];
				}
				itemOrderd[qtyOrdered] = null;
				break;
			}
		}
	}

	public float totalCost() {
		float sum = 0;

		// for (DigitalVideoDisc digitalVideoDisc : itemOrderd) {
		// sum += digitalVideoDisc.getCost();
		// }
		for (int i = 0; i < this.qtyOrdered; i++) {
			sum += itemOrderd[i].getCost();
		}
		return sum;
	}
}
