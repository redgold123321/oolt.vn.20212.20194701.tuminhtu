/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author DELL
 */

import java.util.Scanner;

public class Sort_Numeric {
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);
        int n = 0;
        do {
            System.out.println("Input lengh : ");
            n = keyboard.nextInt();
            System.out.println("Input array : ");
        } while (n <= 0);
        int arr[] = new int[n];
        for (int i = 0; i < n; i++) {
            int x = keyboard.nextInt();
            arr[i] = x;
        }
        //Sort array
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[i] > arr[j]) {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }

        for (int i : arr){
            System.out.print(i+" ,");
        }
        keyboard.close();
    }
}