/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author DELL
 */
import java.util.Scanner;

public class Add_two_matrices {
    public static void main(String[] args){
        int x, y;
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Rows of matrix");
        x = keyboard.nextInt();
        System.out.println("Columns of matrix");
        y = keyboard.nextInt();
        int array1[][] = new int[x][y];
        int array2[][] = new int[x][y];
        int sum[][] = new int[x][y];
        System.out.println("First matrix:");        
        for (int i = 0; i < x; i++)
            for (int j = 0; j < y; j++)
                array1[i][j] = keyboard.nextInt();

        System.out.println("Second matrix:");

        for (int i = 0; i < x; i++)
            for (int j = 0; j < y; j++)
                array2[i][j] = keyboard.nextInt();

        for (int i = 0; i < x; i++)
            for (int j = 0; j < y; j++)
                sum[i][j] = array1[i][j] + array2[i][j];

        System.out.println("Sum of two matrix: ");

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++)
                System.out.print(sum[i][j] + " ");

            System.out.println();
        }
        keyboard.close();
    }

}