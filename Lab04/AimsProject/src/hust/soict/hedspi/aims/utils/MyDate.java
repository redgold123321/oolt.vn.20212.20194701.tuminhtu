package hust.soict.hedspi.aims.utils;

import java.time.LocalDate;

public class MyDate {
  private int date, month, year;

  public MyDate() {
    LocalDate today = java.time.LocalDate.now();
    this.date = today.getDayOfMonth();
    this.month = today.getMonthValue();
    this.year = today.getYear();
  }

  public static int checkMonth(String month) {
    // System.out.println(month.equals("1"));
    return switch (month) {
      case "1", "January", "Jan.", "Jan" ->
        // System.out.println("thang 1");
              1;
      case "2", "February", "Feb.", "Feb" -> 2;
      case "3", "March", "Mar.", "Mar" -> 3;
      case "4", "April", "Apr.", "Apr" -> 4;
      case "5", "May" -> 5;
      case "6", "June", "Jun" -> 6;
      case "7", "July", "Jul" -> 7;
      case "8", "August", "Aug.", "Aug" -> 8;
      case "9", "September", "Sept.", "Sep" -> 9;
      case "10", "October", "Oct.", "Oct" -> 10;
      case "11", "November", "Nov.", "Nov" -> 11;
      case "12", "December", "Dec.", "Dec" -> 12;
      default -> 0;
    };
  }

  public MyDate(String date) {
    String[] split = date.split(" ");
    this.month = checkMonth(split[0]);

    // String d = split[1].substring(0, 2);
    this.date = Integer.parseInt(split[1], 0, split[1].length() - 2, 10);
    this.year = Integer.parseInt(split[2]);
  }

  public MyDate(int date, int month, int year) {
    this.date = date;
    this.month = month;
    this.year = year;
  }

  public String toString() {
    return date + "/" + month + "/" + year;
  }

  public void print() {
    System.out.println("DAY/MONTH/YEAR\n" + date + "/" + month + "/" + year);
  }

  public int getDate() {
    return date;
  }

  public void setDate(int date) {
    this.date = date;
  }

  public int getMonth() {
    return month;
  }

  public void setMonth(int month) {
    this.month = month;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }
}
