package hust.soict.hedspi.aims.disc;

public class CompactDisc {
    private String title;
    private String category;
    private String directory;

    private int length;
    private float cost;

    public CompactDisc() {
    }

    public CompactDisc(String title, String category, String directory) {
        this.title = title;
        this.category = category;
        this.directory = directory;
    }

    public CompactDisc(String title) {
        this.title = title;
    }

    public CompactDisc(String title, String category, String directory, int length, float cost) {
        this.title = title;
        this.category = category;
        this.directory = directory;
        this.length = length;
        this.cost = cost;
    }

    public void print() {
        System.out.println("CD - " + title + " - " +category +
                " - " +directory + " - " +length+" : " + cost
        );
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }
}
