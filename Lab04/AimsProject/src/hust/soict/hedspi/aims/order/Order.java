package hust.soict.hedspi.aims.order;

import hust.soict.hedspi.aims.disc.CompactDisc;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.utils.MyDate;

import java.util.Random;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nbOrders = 0;
	private int qtyOrdered = 0;
	private int qtyCdOrdered = 0;
	private int qtyDvdOrdered = 0;

	private Order() {
	}
	public static Order createOrder() {
		if (nbOrders < MAX_LIMITED_ORDERS) {
			nbOrders++;
			return new Order();
		} else {
			System.out.println("You have reached max orders");
			return null;
		}
	}
	private DigitalVideoDisc dvdOrderd[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private CompactDisc cdOrdered[] = new CompactDisc[MAX_NUMBERS_ORDERED];
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered + 1 <= MAX_NUMBERS_ORDERED ) {
			dvdOrderd[qtyDvdOrdered++] = disc;
			qtyOrdered++;
			System.out.println("The disc has been added");
		} else {
			System.out.println("The order is almost full");
		}
	}

	public void addCompactDisc(CompactDisc disc) {
		if (qtyOrdered + 1 <= MAX_NUMBERS_ORDERED) {
			cdOrdered[qtyCdOrdered++] = disc;
			qtyOrdered++;
			System.out.println("The disc has been added");
		} else {
			System.out.println("The order is almost full");
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList){
		int length = dvdList.length;
		if (qtyOrdered + length <= MAX_NUMBERS_ORDERED) {
			for (DigitalVideoDisc digitalVideoDisc : dvdList) {
				dvdOrderd[qtyDvdOrdered++] = digitalVideoDisc;
				qtyOrdered++;
			}
		} else {
			System.out.println("The order is almost full");
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {

		for (int i = 0; i < qtyOrdered; i++) {
			if (dvdOrderd[i].equals(disc)) {
				for (int j = i; j < qtyOrdered - 1; j++) {
					dvdOrderd[j] = dvdOrderd[j + 1];
				}
				dvdOrderd[qtyOrdered] = null;
				break;
			}
		}
	}

	public float totalCost() {
		float sum = 0;

		// for (hust.soict.hedspi.aims.disc.DigitalVideoDisc digitalVideoDisc : itemOrderd) {
		// sum += digitalVideoDisc.getCost();
		// }
		for (int i = 0; i < this.qtyDvdOrdered; i++) {
			sum += dvdOrderd[i].getCost();
		}
		for (int i = 0; i < qtyCdOrdered; i++) {
			sum += cdOrdered[i].getCost();
		}
		return sum;
	}

	public void print() {
		System.out.println("***********************Order***********************");
		System.out.println("Date : " + new MyDate());
		int pos = 1;
		for (int i = 0; i < qtyDvdOrdered; i++) {
			System.out.print(pos++ + ".");dvdOrderd[i].print();
		}
		for (int i = 0; i < qtyCdOrdered; i++) {
			System.out.print(pos++ + ".");
			cdOrdered[i].print();
		}
		System.out.println("Total cost : " + totalCost());
		System.out.println("***************************************************");
	}

	public void findByTitle(String title) {
		for (int i = 0; i < qtyDvdOrdered; i++) {
			 DigitalVideoDisc dvd = dvdOrderd[i];
			if ( dvd.searchByTitle(title)) dvd.print();
		}
	}

	public DigitalVideoDisc getALuckyItem() {
//		Random ran = new Random();
//		int luckyNumber = ran.nextInt(qtyDvdOrdered);
		System.out.println("Get a lucky item : ");
		return dvdOrderd[new Random().nextInt(0, qtyDvdOrdered)];
	}
}
